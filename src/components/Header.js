import React from 'react'
import User from './User'


const Header = ({ titleClickHandler, userClickHandler, user, page, handleNavClick }) => {

    const headerStyle = {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "baseline",
        background: "linear-gradient(to right, rgba(0,0,0,0.1), rgba(0,0,0,0.9))",
        paddingTop: "20px",
        paddingBottom: "20px",
    }

    const titleStyle = {
        color: "yellow",
        cursor: "pointer",
        fontSize: "36px",
        fontWeight: "800",
        textShadow: "2px 2px orange"
    }

    const pagesStyle = {
        display: "flex",
    }

    const pageStyle = {
        marginRight: "20px",
        fontSize: "20px",
        fontWeight: "600",
        color: "rgba(0,0,0,0.5)",
        cursor: "pointer",
        marginTop: "10px",
    }

    const currentPage = {
        marginRight: "20px",
        fontSize: "20px",
        fontWeight: "600",
        color: "black",
        cursor: "pointer",
        marginTop: "10px",
        borderBottom: "solid 3px yellow",
        paddingBottom: "3px",
    }

    const navStyle = {
        marginLeft: "100px",
        display: "flex",
    }

    return (
        <header style={headerStyle}>
            <div style={navStyle}>

                <div>
                    <h1 onClick={titleClickHandler} style={titleStyle}> Lost in Translation </h1>
                    <div style={pagesStyle}>
                        <div style={page === "Login" ? currentPage : pageStyle} onClick={handleNavClick}> Login </div>
                        <div style={page === "Translation" ? currentPage : pageStyle} onClick={handleNavClick}> Translate </div>
                        <div style={page === "User information" ? currentPage : pageStyle} onClick={handleNavClick}> User </div>
                    </div>
                </div>
            </div >

            <User
                userClickHandler={userClickHandler}
                user={user}
            />
        </header>
    )
}

export default Header