import React, { useEffect, useState } from 'react';
import Sign from './Sign';

const TranslateResult = ({ signs }) => {

    const [images, setImages] = useState([])

    useEffect(() => {

        const importAll = (r) => {
            return r.keys().map(r);
        }
        setImages(importAll(require.context('../images/individual_signs/', false, /\.(png)$/)))

    }, [])


    const resultStyle = {
        overflow: "hidden",
        width: "80vw",
        marginTop: "50px",
        padding: "20px",
        marginBottom: "-100px",
        borderRadius: "30px",
        display: "flex",
        justifyContent: "center",
        flexWrap: "wrap",
    }


    if (signs.length > 0) {
        return (
            <div style={resultStyle}>
                {signs.map(sign => {
                    return <Sign key={sign.id} sign={sign.letter} image={images[sign.letter.charCodeAt(0) - 97]} />
                })}
            </div>
        )
    }
    else return null
}

export default TranslateResult