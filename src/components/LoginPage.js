import React from 'react';
import LoginForm from './LoginForm'

const LoginPage = ({ onUsernameChanged, onNewUserClicked, user, onChangeUserClick, onCurrentUserClick }) => {

    const loginStyle = {
        display: "flex",
        width: "100%",
        height: "100%",
        flexDirection: "column", 
    }

    return (
        <div style={loginStyle}>
            <LoginForm
                onUsernameChanged={onUsernameChanged}
                onNewUserClicked={onNewUserClicked}
                onChangeUserClick={onChangeUserClick}
                onCurrentUserClick={onCurrentUserClick  }
                user={user} />
        </div>
    )
}

export default LoginPage