import React from 'react';
import userImg from "../images/user.png"

const User = ({userClickHandler, user}) => {

    const userStyle = {
        margin: "auto",
        marginRight: "100px",
        marginLeft: "0px",
        display: "flex",
        justifyContent: "baseline",
        cursor: "pointer",
        padding: "5px",
        paddingLeft: "15px",    
        boxShadow: "3px 3px 0px orange",
        borderRadius: "15px",
        backgroundColor: "yellow",
    }

    const usernameStyle = {
        margin: "auto",
    }
    
    const imageStyle = {
        width: "70px",
        
    }

    if (user) {
        return (
            <div style={userStyle} onClick={userClickHandler}>
                <p style={usernameStyle}> {user.username} </p>
                <img src={userImg} alt={user.username} style={ imageStyle }/>
            </div>
        )
    }
    else return <div> </div>
}

export default User