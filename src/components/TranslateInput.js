import React from 'react';

const TranslateInput = ({ translateTextHandler, translateButtonClicked }) => {


    const divStyle = {
        margin: "auto"
    }

    const inputStyle = {
        padding: "15px",
        paddingRight: "100px",
        outline: "none",
        fontSize: "20px",
        border: "solid 2px gray",
        color: "rgba(0,0,0,0.5)",
        borderTopLeftRadius: "10px",
        borderBottomLeftRadius: "10px",
    }

    const buttonStyle = {
        padding: "15px",
        outline: "none",
        backgroundColor: "yellow",
        fontWeight: "700",
        fontSize: "20px",
        color: "rgba(0,0,0,0.5)",
        border: "solid 2px gray",
        borderLeft: "none",
        borderTopRightRadius: "10px",
        borderBottomRightRadius: "10px",
    }


    return (
        <div style={divStyle}>

            <input onChange={translateTextHandler} style={inputStyle} type="text" placeholder="Translate something..."/>
            <button onClick={translateButtonClicked} style={buttonStyle}> Translate </button>

        </div>
    )
}

export default TranslateInput