import React, { useState } from 'react'
import TranslateInput from './TranslateInput'
import TranslateResult from './TranslateResult'
import { setStorage, getStorage } from '../utils/storage'

const TranslatePage = ({ user }) => {


    const [signs, setSigns] = useState([])
    const [translateText, setTranslateText] = useState("")

    const translateTextHandler = e => setTranslateText(e.target.value)
    
    const translateButtonClicked = () => {
        if (translateText.trim() === ""){
            window.alert("Cannot be empty!")
        }
        else if (translateText.length > 20){
            window.alert("Cannot translate text more than 20 letters!")
        }  
        else translate(translateText)
    }
        
    const translate = text => {

        storeText(text)

        const signList = []

        for (let i = 0; i < text.length; i++){

            const char = text.charAt(i).toLowerCase()

            if (char.charCodeAt(0) >= 97 && char.charCodeAt(0) <= 122){
                // letter is in a-z
                signList.push({
                    id: signList.length,
                    letter: char,
                })
            }
            else if (char.charCodeAt(0) === 32){
                signList.push({
                    id: signList.length,
                    letter: "space"
                })
            }
            else {
                signList.push({
                    id: signList.length,
                    letter: ""
                })
            }
        }

        setSigns(signList)
    }

    const storeText = text => {

        const userFromStorage = getStorage("user")
        
        
        userFromStorage.textsTranslated.push(text)

        if (userFromStorage.textsTranslated.length > 10){
            userFromStorage.textsTranslated.shift()
        }

        console.log(userFromStorage)
        setStorage("user", userFromStorage)
    

    }

    const translateStyle = {
        position: "absolute",
        display: "flex",
        justifyContent: "center",
        flexDirection: "column",
        top: "50%",
        left: "50%",
        transform: "translate(-50%,-50%)",
    }

    const textStyle = {
        fontSize: "32px",
        color: "rgba(0,0,0,0.6)",
    }

    if (!user) {
        return (
            <div style={translateStyle}>
                <h5 style={textStyle}> Must be logged in to translate </h5>
            </div>
        )
    }
    else return (
        <div style={translateStyle}>
            <TranslateInput
                translateButtonClicked={translateButtonClicked}
                translateTextHandler={translateTextHandler} />
            <TranslateResult
                signs={signs}

            />
        </div>
    )
}

export default TranslatePage