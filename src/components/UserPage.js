import React, { useState, useEffect } from 'react'
import { getStorage } from '../utils/storage'

const UserPage = ({ logoutClickHandler, user }) => {

    const [textsTranslated, setTextsTranslated] = useState([])

    useEffect(() => {
        const u = getStorage("user")
        if (u){
            setTextsTranslated(u.textsTranslated)
        } 
    }, [])

    const userStyle = {
        position: "absolute",
        display: "flex",
        justifyContent: "center",
        flexDirection: "column",
        top: "50%",
        left: "50%",
        transform: "translate(-50%,-50%)",
    }

    const textStyle = {
        fontSize: "32px",
        color: "rgba(0,0,0,0.6)",
    }

    const transStyle = {
        margin: "auto",
        padding: "1px",
        paddingRight: "20px",
        paddingLeft: "20px",
        color: "rgba(0,0,0,0.6)",
        fontWeight: "500",
        borderRadius: "10px",
    }

    const titleStyle = {
        margin: "auto",
        marginBottom: "15px",
        color: "rgba(0,0,0,0.5)",
        fontSize: "26px",
        fontWeight: "700",
    }

    const buttonStyle = {
        marginTop: "30px",
        marginBottom: "-100px",
        padding: "20px",
        paddingRight: "100px",
        paddingLeft: "100px",
        borderRadius: "10px",
        border: "none",
        backgroundColor: "yellow",
        fontWeight: "700",
        fontSize: "20px",
        color: "rgba(0,0,0,0.5)",
        boxShadow: "3px 3px 0px orange",
        cursor: "pointer"
    }

    if (user && textsTranslated.length === 0){
        return (
            <div style={userStyle}>

                <h2 style={titleStyle}> No translated texts </h2>
                <button onClick={logoutClickHandler} style={buttonStyle}> LOGOUT </button>

            </div>
        )   
    }
    else if (user) {
        return (
            <div style={userStyle}>

                {textsTranslated.length > 0 && <h2 style={titleStyle}> Translated texts </h2>}

                {textsTranslated.map((text, index) => {
                    return <div key={index} style={transStyle}> {text} </div>
                })}

                <button onClick={logoutClickHandler} style={buttonStyle}> LOGOUT </button>

            </div>
        )
    }
    else return (
        <div style={userStyle}>
            <h5 style={textStyle}> Must be logged in to view user information </h5>
        </div>
    )
}

export default UserPage