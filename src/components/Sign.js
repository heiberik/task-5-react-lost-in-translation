import React from 'react';

const Sign = ({ sign, image }) => {




    const signStyle = {

    }

    const signStyleSpace = {
        paddingRight: "50px",
    }

    const imageStyle = {
        width: "60px",
    }

    if (sign === "space") {
        return (
            <div style={signStyleSpace}></div>
        )
    }
    else if (sign === "") return null
    else return (
        <div style={signStyle}>
            <img src={image} style={imageStyle} alt="sign" />
        </div>
    )
}

export default Sign