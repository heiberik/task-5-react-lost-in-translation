import React from 'react';


const LoginForm = ({ onUsernameChanged, onNewUserClicked, onChangeUserClick, user, onCurrentUserClick }) => {

    
    const loginStyle = {
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%,-50%)",
        boxShadow: "1px 5px 20px gray",
        borderRadius: "10px",
        backgroundColor: "gray",
    }

    const inputStyle = {
        padding: "15px",
        paddingRight: "100px",
        outline: "none",
        fontSize: "20px",
        border: "solid 2px gray",
        color: "rgba(0,0,0,0.5)",
        borderTopLeftRadius: "10px",
        borderBottomLeftRadius: "10px",
    }

    const buttonStyle = {
        padding: "15px",
        outline: "none",
        backgroundColor: "yellow",
        fontWeight: "700",
        fontSize: "20px",
        color: "rgba(0,0,0,0.5)",
        border: "solid 2px gray",
        borderLeft: "none",
        borderTopRightRadius: "10px",
        borderBottomRightRadius: "10px",
    }

    const buttonStyleLeft = {
        padding: "15px",
        outline: "none",
        backgroundColor: "yellow",
        fontWeight: "700",
        fontSize: "20px",
        paddingRight: "30px",
        color: "rgba(0,0,0,0.5)",
        border: "solid 2px gray",
        borderTopLeftRadius: "10px",
        borderBottomLeftRadius: "10px",
    }

    if (!user) {
        return (
            <div style={loginStyle}>
                <input type="text" placeholder="Choose your username" style={inputStyle} onChange={onUsernameChanged} />
                <button style={buttonStyle} onClick={onNewUserClicked}> Login </button>
            </div>
        )
    }
    else return (
        <div style={loginStyle}>
            <button style={buttonStyleLeft} onClick={onChangeUserClick}> Login with new user </button>
            <button style={buttonStyle} onClick={onCurrentUserClick}> Login with current user </button>
        </div>
    )
}

export default LoginForm