import React, { useState, useEffect } from 'react'
import { BrowserRouter as Route, Switch, useHistory } from "react-router-dom"
import { getStorage, setStorage, removeStorage } from './utils/storage'

import LoginPage from './components/LoginPage'
import TranslatePage from './components/TranslatePage'
import UserPage from './components/UserPage'
import Header from './components/Header'

function App() {

	const [page, setPage] = useState("Login")
	const [newUserText, setNewUserText] = useState("")
	const [user, setUser] = useState(null)

	const history = useHistory()

	useEffect(() => {
		const userFromLS = getStorage("user")
		if (userFromLS) {
			setUser(userFromLS)
		}

		const path = window.location.pathname
		if (path.includes("translate")) setPage("Translation")
		else if (path.includes("user")) setPage("User information")
		else setPage("Login")

	}, [])


	const onChangeUserClick = () => {
		removeStorage("user")
		setUser(null)
	}

	const onUsernameChanged = e => setNewUserText(e.target.value)

	const titleClickHandler = () => {
		setPage("Login")
		history.push('/')
	}

	const userClickHandler = () => {
		setPage("User information")
		history.push("/user")
	}

	const onCurrentUserClick = () => {
		setPage("Translation")
		history.push('/translate')
	}

	const onNewUserClicked = () => {

		if (newUserText.trim() !== "") {

			const newU = {
				username: newUserText.trim(),
				textsTranslated: []
			}

			setStorage("user", newU)
			setUser(newU)
			setPage("Translation")
			history.push('/translate')
		}
		else {
			window.alert("Username cannot be empty!")
		}
	}

	const logoutClickHandler = () => {
		setPage("Login")
		removeStorage("user")
		setUser(null)
		history.push('/')
	}

	const handleNavClick = e => {

		if (e.target.innerHTML.trim() === "Login") {
			setPage("Login")
			history.push('/')
		}
		if (e.target.innerHTML.trim() === "Translate") {
			setPage("Translation")
			history.push('/translate')
		}
		if (e.target.innerHTML.trim() === "User") {
			setPage("User information")
			history.push('/user')
		}
	}


	return (

		<div>
			<Header
				page={page}
				handleNavClick={handleNavClick}
				user={user}
				userClickHandler={userClickHandler}
				titleClickHandler={titleClickHandler} />

			<Switch>

				<Route exact path="/">
					<LoginPage
						onUsernameChanged={onUsernameChanged}
						onNewUserClicked={onNewUserClicked}
						onChangeUserClick={onChangeUserClick}
						onCurrentUserClick={onCurrentUserClick}
						user={user} />
				</Route>

				<Route path="/translate">
					<TranslatePage
						user={user} />
				</Route>

				<Route path="/user">
					<UserPage
						user={user}
						logoutClickHandler={logoutClickHandler} />
				</Route>
			</Switch>
		</div>

	)
}

export default App;
